#
#  Copyright (C) 2020 Codethink Limited
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt>.
#
#  Authors:
#        Darius Makovsky <darius.makovsky@codethink.co.uk>
"""Bazelise kind Buildstream element plugin

It creates BUILD files calling bazel
[cc_* rules](https://docs.bazel.build/versions/master/be/c-cpp.html).

As an example considering an element `makelib.bst` producing an artifact
containing:
    * usr/lib/lib.so
    * usr/include/hdr.h

An element of this kind ('bazelize.bst') declaring a
`build-depends: makelib.bst` will produce a BUILD file containing:

```
load("@rules_cc//cc:defs.bzl", "cc_library")

cc_library(
    name = "makelib",
    srcs = ['usr/lib/lib.so'],
    hdrs = ['usr/include/hdr.h']
)
```
"""
import re
import os

from typing import (
    Generator,
    List,
    Set,
    Optional,
    Tuple,
    TYPE_CHECKING,
)
from buildstream import (  # pylint: disable=import-error
    Element,
    Scope,
    MappingNode,
)

if TYPE_CHECKING:
    from buildstream.types import SourceRef  # pylint: disable=import-error
    from buildstream import Sandbox  # pylint: disable=import-error

# header regex
BAZELIZE_HDR_RE = re.compile(r"^.*\.h$")
# source regex
BAZELIZE_SOURCE_RE = re.compile(r"^.*\.(c(pp)?|so|a)$")


class BazelRuleEntry:  # pylint: disable=too-few-public-methods
    """Simple class to hold information about cc_* bazel rule targets and
    information relevant to harvesting this from the element"""

    # header regex
    HDR_RE = BAZELIZE_HDR_RE
    # source regex
    SOURCE_RE = BAZELIZE_SOURCE_RE
    # the empty metarule
    NONE_RULE = "BST.BAZEL_NONE_RULE"
    # default rule
    DEFAULT_RULE = "cc_library"

    @staticmethod
    def _strip_element_name(name):
        """Remove the .bst substring from names for aesthetic reasons"""
        if name.endswith(".bst"):
            name = name[:-4]
        return name

    @staticmethod
    def get_directive(rules: Set[str]) -> str:
        """Formats a set of strings into a string expressing a bazel load
        directive from a standard rule definition and returns this string."""
        load_str = str()
        # discard the empty rule
        rules.discard(BazelRuleEntry.NONE_RULE)

        # return empty str if there are no rules
        if not rules:
            return load_str

        for rule in sorted(list(rules)):
            load_str += ', "{}"'.format(rule)
        return 'load("@rules_cc//cc:defs.bzl"{})'.format(load_str) + os.linesep

    def __init__(
        self,
        element: Element,
        manifest: Optional[Generator[str, None, None]] = None,
    ) -> None:
        self.name = BazelRuleEntry._strip_element_name(element.name)
        self.bazel_rule: str = BazelRuleEntry.DEFAULT_RULE
        self.srcs: List[str] = []
        self.hdrs: List[str] = []
        self.deps: List[str] = []
        self.copts: List[str] = []

        if element.get_kind() == "bazelize":
            self.bazel_rule = element.bazel_rule
            self.copts = element.copts

        # empty rules have no semantic meaning for the BUILD
        if self.bazel_rule == BazelRuleEntry.NONE_RULE:
            return

        # sources and headers from manifest and element.sources
        _srcs = set()
        for source in element.sources():
            _srcs.add(os.path.basename(source.path))
        self.srcs = list(_srcs)
        del _srcs

        # get target names of deps from element.dependencies
        _deps = set()
        for dep in element.dependencies(Scope.BUILD, recurse=False):
            _deps.add(BazelRuleEntry._strip_element_name(dep.name))
        self.deps = sorted(list(_deps))
        del _deps

        if manifest:
            self._match_manifest_items(manifest)

        # sort headers and sources
        self.srcs.sort()
        self.hdrs.sort()
        return

    def _match_manifest_items(
        self, manifest: Generator[str, None, None]
    ) -> None:
        srcs = set()
        hdrs = set()
        for item in manifest:
            _maybe = re.match(BazelRuleEntry.SOURCE_RE, item)
            if _maybe:
                # the item looks like a source
                srcs.add(item)
            else:
                _maybe = re.match(BazelRuleEntry.HDR_RE, item)
                if _maybe:
                    # the item looks like a header
                    hdrs.add(item)
        self.srcs += list(srcs)
        self.hdrs += list(hdrs)

    def __str__(self) -> str:
        """Implementation for representing the entry"""
        # avoid representing the empty targets
        if self.bazel_rule == BazelRuleEntry.NONE_RULE:
            return str()

        msg = (
            "{}(".format(self.bazel_rule)
            + os.linesep
            + '    name = "{}",'.format(self.name)
            + os.linesep
        )
        if self.srcs:
            msg += "    srcs = {},".format(self.srcs) + os.linesep
        if self.hdrs:
            msg += "    hdrs = {},".format(self.hdrs) + os.linesep
        if self.deps:
            msg += "    deps = {},".format(self.deps) + os.linesep
        if self.copts:
            msg += "    copts = {},".format(self.copts) + os.linesep
        msg += ")" + os.linesep
        return msg


class BazelizeElement(Element):
    """Buildstream element plugin kind formatting calls to cc_library rules"""

    # not permitted to have runtime dependencies
    BST_FORBID_RDEPENDS = True

    def preflight(self) -> None:
        # the caller currently raises ElementError if sources or runtime
        # dependencies are provided
        pass

    def stage(self, sandbox: "Sandbox") -> None:
        pass

    def configure_sandbox(self, sandbox: "Sandbox") -> None:
        pass

    def configure(self, node: MappingNode) -> None:
        # configure the path for the BUILD file and some options
        node.validate_keys(["buildfile-path", "copts", "bazel-rule"])

        self.build_file_path = self.node_subst_vars(  # pylint: disable=attribute-defined-outside-init
            node.get_scalar("buildfile-path")
        )

        self.copts = self.node_subst_sequence_vars(  # pylint: disable=attribute-defined-outside-init
            node.get_sequence("copts")
        )
        # sort the options to gaurantee a unique key
        self.copts.sort()

        # get the rule for this element
        self.bazel_rule = self.node_subst_vars(  # pylint: disable=attribute-defined-outside-init
            node.get_scalar("bazel-rule")
        )

    def get_unique_key(self) -> "SourceRef":
        return {
            "buildfile-path": self.build_file_path,
            "copts": self.copts,
            "bazel-rule": self.bazel_rule,
        }

    @staticmethod
    def _gather_target(
        element: Element, manifest: Optional[Generator[str, None, None]] = None
    ) -> BazelRuleEntry:
        return BazelRuleEntry(element, manifest)

    def _gather_targets(self) -> Tuple[str, List[BazelRuleEntry]]:
        """Gather the required rules for the defined targets

           This returns a list of rule entry objects and a load directive str.
        """
        targets_set: Set[BazelRuleEntry] = set()

        for dep in self.dependencies(Scope.BUILD, recurse=False):
            target = BazelizeElement._gather_target(
                dep, dep.compute_manifest()
            )
            # collect the target only if it's not an empty rule
            if target.bazel_rule != BazelRuleEntry.NONE_RULE:
                targets_set.add(target)

        # collect the element only if it's not an empty rule
        if self.bazel_rule != BazelRuleEntry.NONE_RULE:
            targets_set.add(BazelizeElement._gather_target(self))

        # sort by target name
        targets: List[BazelRuleEntry] = sorted(
            list(targets_set), key=lambda x: x.name
        )
        del targets_set

        rule_types = set()
        for target in targets:
            rule_types.add(target.bazel_rule)
        load_directive = BazelRuleEntry.get_directive(rule_types)

        return load_directive, targets

    def assemble(self, sandbox: "Sandbox") -> str:
        # gather the sorted list of targets and the load directive
        load_directive, targets = self._gather_targets()

        # attempt to write the BUILD file from assembled rule entries
        basedir = sandbox.get_directory()
        build_file_path = os.path.join(
            basedir, self.build_file_path.lstrip(os.path.sep)
        )
        build_file_dir = os.path.dirname(build_file_path)
        os.makedirs(build_file_dir, exist_ok=True)

        with open(build_file_path, "w") as buildf:
            # write the load directives
            buildf.write(load_directive)
            for target in targets:
                buildf.write(str(target))
        return os.path.sep


def setup():
    return BazelizeElement
