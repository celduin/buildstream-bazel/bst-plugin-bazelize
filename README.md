# bazelize
This plugin facilitates the limited integration of buildstream artifacts into bazel projects. It serves as tooling providing bazel BUILD metadata (specifying buildstream artifacts) as part of the buildstream build output.

The intended use is as the kind of a dummy element with a build-depends field declaring elements required by a bazel project. The execution of the buildstream build action will produce a single BUILD file specifying the consumption of the relevant artifacts of those dependencies. In this way the artifacts will be immediately useful to a bazel project without additional development effort.

## Issues
Issues for this repo are now being tracked at the buildstream-bazel [issue-tracker](https://gitlab.com/celduin/buildstream-bazel/issue-tracker/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=bst-plugin-bazelize)

## implementation
This must not depend upon bazel since that is the intended consumer. The plugin will, given a list of `build-depends`, attempt to harvest a list of files in the artifact of each. Each manifest will be globbed for library-like names (`*.so`, `*.h`, `*.a`). The list of matching paths in the manifest will be keyed to the element. The resulting dictionary entries will be transformed into a BUILD-like entry. That entry will be written in terms of bazel rules (with paths relative to the target configured in the bazelizing plugin) and appended to a BUILD file in the artifact of the bazelizing plugin.

eg. an element called `gtk.bst` producing `/lib/libgtk-3.so` and `/include/gtk-3.0/gtk/foo.h`, when listed as a build-depends of an element 'bazelize.bst' of this plugin kind will produce a BUILD:

```
load("@rules_cc//cc:defs.bzl", "cc_library")
load("@rules_cc//cc:defs.bzl", "cc_binary")

cc_library(
    name = "gtk",
    srcs = ["/lib/libgtk-3.so"],
    hdrs = glob(["/include/gtk-3.0/gtk/*.h"])
)

cc_binary(
    name = "bazelize",
    srcs = [],
    deps = ['gtk'],
    copts = []
)
```
