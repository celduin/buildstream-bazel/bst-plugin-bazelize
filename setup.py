#!/usr/bin/env python3

import os
import sys

try:
    from setuptools import setup, find_packages
except ImportError:
    print("BuildStream requires setuptools in order to locate plugins. Install "
          "it using your package manager (usually python3-setuptools) or via "
          "pip (pip3 install setuptools).")
    sys.exit(1)

###############################################################################
#                             Parse README                                    #
###############################################################################
with open(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                       'README.md')) as readme:
    long_description = readme.read()


setup(name='bst-plugin-bazelize',
      version="0.0.1",
      description="A BuildStream plugin for generating BUILD rules.",
      long_description=long_description,
      long_description_content_type='text/x-rst; charset=UTF-8',
      license='LGPL',
      url='https://gitlab.com/celduin/buildstream-bazel/bst-plugin-bazelize',
      package_dir={'': 'src'},
      packages=find_packages(where='src'),
      include_package_data=True,
      entry_points={
          'buildstream.plugins.elements': [
              'bazelize = bst_plugin_bazelize.elements.bazelize'
          ]
      },
      zip_safe=False)
# eof setup()
